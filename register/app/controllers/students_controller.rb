class StudentsController < ApplicationController

  def index
    @students = Student.all
  end

  def new
  end

  def create
    #render plain: params[:student].inspect
    @newstudent = Student.new student_params
    @newstudent.save

    redirect_to @newstudent
  end

  def show
    @student = Student.find params[:id]
  end

  private
  def student_params
    params.require(:student).permit(:name, :lastname, :birthdate, :career)
  end

end
